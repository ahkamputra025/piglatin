function PigLatin(str) {
    let vowels = ['a', 'e', 'i', 'o', 'u'];
    str = str.toLowerCase();
    
    let newStr = "";
    if (vowels.indexOf(str[0]) > -1) {
        newStr = str;
        return newStr;
    } else {
        let firstMatch = str.match(/[aeiou]/g) || 0;
        a = str.split(' ');
        
        if(a.length > 1){
            for(let i=0; i<a.length; i++){
                let vowel = str.indexOf(firstMatch[0]);
                let ab = a[i].substring(vowel);
                let ac = a[i].substring(0,vowel);
                newStr += ab+ac+"ay ";
            }
        }else{
            let vowel = str.indexOf(firstMatch[0]);
            newStr = str.substring(vowel) + str.substring(0, vowel) + "ay";
        }
        return newStr;
    }
}
console.log(PigLatin('food')) // ---> oodfay
console.log(PigLatin('snap')) // ---> apsnay
console.log(PigLatin('guide')) // ---> uidegay
console.log(PigLatin('beli makanan')) // ---> elibay akananmay
console.log(PigLatin('apel')) // ---> apel